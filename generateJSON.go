package main

import (
	"encoding/json"
	"fmt"
	shapesModule "gitlab.com/alikhan.murzayev/lesson-5-module"
	"io/ioutil"
	"math/rand"
	"strconv"
)

func main() {
	//defer TrackTime(time.Now(), "Dataset generation")

	datasetSize := 1000000
	fileName := "dataset.json"
	shapes := GenerateDataset(datasetSize)
	shapesJson, err := json.Marshal(shapes)
	if err != nil {
		panic(err)
	}
	err = ioutil.WriteFile(fileName, shapesJson, 0666)
	if err != nil {
		panic(err)
	}
	fmt.Println("Complete")
}

func GenerateDataset(datasetSize int) shapesModule.Shapes {
	var shapes shapesModule.Shapes
	for i := 0; i < datasetSize; i++ {
		shape := GenerateShape()
		shapes.Shapes = append(shapes.Shapes, shape)
	}
	return shapes
}

func GenerateShape() shapesModule.GeneralShape {
	shape := shapesModule.GeneralShape{}

	shapeType := rand.Intn(4)
	switch shapeType {
	case 0:
		shape.Type = "circle"
		radiusType := rand.Intn(4)
		shape.R = GenerateValue(radiusType)
	case 1:
		shape.Type = "square"
		sideAType := rand.Intn(4)
		shape.SideA = GenerateValue(sideAType)
	case 2:
		shape.Type = "rectangle"
		sideAType := rand.Intn(4)
		sideBType := rand.Intn(4)
		shape.SideA = GenerateValue(sideAType)
		shape.SideB = GenerateValue(sideBType)
	case 3:
		shape.Type = "triangle"
		sideAType := rand.Intn(4)
		sideBType := rand.Intn(4)
		sideCType := rand.Intn(4)
		shape.SideA = GenerateValue(sideAType)
		shape.SideB = GenerateValue(sideBType)
		shape.SideC = GenerateValue(sideCType)
	}
	return shape
}

func GenerateValue(valueType int) interface{} {
	maxValue := 1000
	switch valueType {
	case 0:
		return 1 + rand.Intn(maxValue)
	case 1:
		return 1 + rand.Float64()
	case 2:
		return strconv.Itoa(1 + rand.Intn(maxValue))
	case 3:
		return fmt.Sprintf("%f", 1+rand.Float64())
	default:
		return "Wrong value!"
	}
}

//func TrackTime(startTime time.Time, name string) {
//	elapsed := time.Since(startTime)
//	fmt.Printf("%s took %s\n", name, elapsed)
//}
