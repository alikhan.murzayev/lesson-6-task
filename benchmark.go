package main

import (
	"encoding/json"
	"fmt"
	shapesModule "gitlab.com/alikhan.murzayev/lesson-5-module"
	"io/ioutil"
	"math"
	"sync"
	"time"
)

func main() {
	fileName := "dataset.json"
	dat, err := ioutil.ReadFile(fileName)
	if err != nil {
		panic(err)
	}

	var shapes shapesModule.Shapes
	err = json.Unmarshal(dat, &shapes)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%d elements\n", len(shapes.Shapes))



	report := Report{}
	reportFileName := "report.json"

	for num := 1; num < 101; num++ {
		for i := 0; i < 10; i++ {
			reportItem := ReportItem{}
			reportItem.NumElements = len(shapes.Shapes)
			reportItem.GoroutinesNum = num
			reportItem.ConsequentialTime = Consequential(&shapes)
			reportItem.ConcurrentTime = Concurrent(&shapes, num)
			report.Add(reportItem)
			fmt.Println(reportItem)
		}
	}
	reportJSON, err := json.Marshal(report)
	if err != nil {
		panic(err)
	}
	err = ioutil.WriteFile(reportFileName, reportJSON, 0644)
	if err != nil {
		panic(err)
	}
}

type ReportItem struct {
	NumElements       int   `json:"elements"`
	ConsequentialTime int64 `json:"consequential"`
	GoroutinesNum     int   `json:"goroutines"`
	ConcurrentTime    int64 `json:"concurrent"`
}

type Report struct {
	ReportItems []ReportItem `json:"reports"`
}

func (report *Report) Add(reportItem ReportItem) {
	report.ReportItems = append(report.ReportItems, reportItem)
}

func Consequential(shapes *shapesModule.Shapes) int64 {
	startTime := time.Now()
	for i := range shapes.Shapes {
		_, _ = shapes.Shapes[i].Area()
	}
	return time.Since(startTime).Nanoseconds()
}

func Concurrent(shapes *shapesModule.Shapes, numRoutines int) int64 {
	startTime := time.Now()
	numShapes := len(shapes.Shapes)
	step := int(math.Floor(float64(numShapes) / float64(numRoutines)))
	waitGroup := sync.WaitGroup{}
	for i := 0; i < numShapes; i += step {

		waitGroup.Add(1)
		go func(waitGroup *sync.WaitGroup, shapes *shapesModule.Shapes, startInd, endInd int) {
			defer waitGroup.Done()
			for ind := startInd; ind < endInd; ind++ {
				_, _ = shapes.Shapes[ind].Area()
			}
		}(&waitGroup, shapes, i, min(i+step, numShapes))
	}
	waitGroup.Wait()
	return time.Since(startTime).Nanoseconds()
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func TrackTime(startTime time.Time, name string) {
	elapsed := time.Since(startTime)
	fmt.Printf("%s took %s\n", name, elapsed)
}
